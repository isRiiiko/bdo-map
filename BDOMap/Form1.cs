﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace BDOMap
{
    public partial class Form1 : Form
    {
        Properties.Settings qwe = new Properties.Settings();
        public Form1()
        {
            InitializeComponent();
            max = qwe.maxWeigh;
            textBox1.Text = max.ToString();
            textBox2.Text = qwe.ubejd.ToString().Replace(",",".");
            checkBox1.Checked = qwe.prem;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// PictureBox на карте, который активен в данный момент (клик по этому PictureBox)
        /// </summary>
        PictureBox nowBox = new PictureBox();

        #region Работа с картой
        /// <summary>
        /// Открытие контекстного меню для выбора итема.
        /// </summary>
        private void openContextMenu(object sender, MouseEventArgs e) 
        {
            nowBox = (PictureBox)sender;
            if (e.Button == MouseButtons.Left)
            {
                contextMenuStrip1.Show(new Point(MousePosition.X, MousePosition.Y), ToolStripDropDownDirection.Default);  
            }
            else if (e.Button == MouseButtons.Right)
            {
                try { listView1.Items[listView1.SelectedIndices[0]+1].Selected = true; }
                catch { }

                delPos();

                nowBox.Image = null;
                toolTip1.SetToolTip(nowBox, null);
            }
        }
        /// <summary>
        /// Выбор итема из списка, добавление в список
        /// </summary>
        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            delPos();
            nowBox.Image = item.Image;
            toolTip1.SetToolTip(nowBox, item.Text);
            addPos(item);
        }
        #endregion

        #region Работа со списком
        private void delPos()
        {
            string key = toolTip1.GetToolTip(nowBox);
            int w = 0;
            try { w = Convert.ToInt32(listView1.Items[key].SubItems[1].Text.Replace("Обменов: ", "")); }
            catch { }
            double u = 0;
            try { u = Convert.ToDouble(listView1.Items[key].SubItems[2].Text); }
            catch { }

            calcBar(-w, -u);

            listView1.Items.RemoveByKey(key);
            lockBoxes();

        }
        private void addPos(ToolStripMenuItem item)
        {
            ListViewItem listViewItem = new ListViewItem(item.Text, Convert.ToInt32(item.Tag));
            listViewItem.Name = item.Text;
            double u = Convert.ToDouble(nowBox.Tag);
            int w = 0;
            //5 lvl
            if (Convert.ToInt32(item.Tag) <= 27 && Convert.ToInt32(item.Tag) >= 14)
                w = 1;
            //4 lvl
            else if (Convert.ToInt32(item.Tag) <= 13)
                w = 3;
            
            listViewItem.SubItems.Add("Обменов: " + w);
            listViewItem.SubItems.Add(u.ToString());

            listView1.Items.Add(listViewItem);
            calcBar(w, u);
            lockBoxes();
        }
        
        /// <summary>
        /// Выделение текущего бартера на карте при выборе элемента в списке
        /// </summary>
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                pictureBoxTarget.Visible = false;
                return;
            }
            foreach (Control control in Controls)
            {
                if (toolTip1.GetToolTip(control) == listView1.SelectedItems[0].Text)
                {
                    pictureBoxTarget.Visible = true;
                    pictureBoxTarget.Location = new Point(control.Location.X-21-13, control.Location.Y-20-13);
                    break;
                }
                else
                    pictureBoxTarget.Visible = false;
            }
        }
        #endregion

        /// <summary>
        /// Просчёт веса и убеждения
        /// </summary>
        private void calcBar(int w, double u) 
        {
            #region Вес
            //Если не 0 рес, и если кол-во обменов не 0 - расчёт веса
            if (nowBox.Name != "other" || w != 0)
            {
                int t = Convert.ToInt32(label1.Text.Replace(" /", "").Replace(" ", "")) + w * 1000;
                if (t < 0) t = 0;
                label1.Text = t.ToString("n").Replace(",00", "") + " /";
            }
            //Текущий вес
            double now = Convert.ToDouble(label1.Text.Replace(" /", ""));
            //Если вес превысил максимум - красный цвет
            if (now >= max)
            {
                panel2.BackColor = Color.DarkRed;
                panel2.Width = panel1.Width;
            }
            //иначе расчёт бара
            else
            {
                panel2.BackColor = Color.FromArgb(173, 130, 87);
                panel2.Width = Convert.ToInt32(now / max * panel1.Width);
            }
            #endregion

            #region Убеждение
            try
            {
                //Расчёт новго убеждения:          Текущее           
                double ubj = Convert.ToDouble(label2.Text.Replace(" ", "")) + rasUbejd(u) * ((nowBox.Name == "other") ? 3 : Math.Abs(w));
                //Просто что бы не было -
                if (ubj < 0) ubj = 0;
                //Если превышен лимит в 1кк - красим красным
                if (ubj > 1000000) label2.ForeColor = label3.ForeColor = Color.DarkRed;
                //Иначе красим обычным цветом
                else label2.ForeColor = label3.ForeColor = Color.FromArgb(221, 195, 158);

                label2.Text = ubj.ToString("n").Split(',')[0];
            }
            catch { }
            #endregion
        }
        /// <summary>
        /// Расчёт необходимого убеждения с учётом скидок
        /// </summary>
        /// <param name="ub">Полное количество убеждения</param>
        /// <returns></returns>
        private double rasUbejd(double ub) 
        {
            double mast = ub * Convert.ToDouble(textBox2.Text.Replace(".", ","))/100;
            double prem = checkBox1.Checked ? ub * 0.1 : 0;
            return ub - mast - prem;
        }
        double max = 0;
        /// <summary>
        /// Изменение максимального веса
        /// </summary>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try { max = Convert.ToDouble(textBox1.Text); if (max == 0) max = 1; qwe.maxWeigh = max; qwe.Save(); }
            catch { textBox1.Text = ""; return; }
            calcBar();
        }
        /// <summary>
        /// Изменение убеждения
        /// </summary>
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try { qwe.ubejd = Convert.ToDouble(textBox2.Text.Replace(".",",")); qwe.Save(); }
            catch { textBox2.Text = ""; }
        }
        /// <summary>
        /// Включение/Отключение према
        /// </summary>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            qwe.prem = checkBox1.Checked; qwe.Save();
        }

        /// <summary>
        /// Обновление бара грузоподъёмности
        /// </summary>
        private void calcBar()
        {
            double now = Convert.ToDouble(label1.Text.Replace(" /", ""));
            if (now >= max)
            {
                panel2.BackColor = Color.DarkRed;
                panel2.Width = panel1.Width;
            }
            else
            {
                panel2.BackColor = Color.FromArgb(173, 130, 87);
                panel2.Width = Convert.ToInt32(now / max * panel1.Width);
            }
        }

        #region Прочий визуал
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            pictureBox1.Focus();
        }
        /// <summary>
        /// Отключение ввода информации о весе и убеждении при наличии элементов в списке
        /// </summary>
        private void lockBoxes() {
            if (listView1.Items.Count == 0)
            {
                textBox1.Enabled = textBox2.Enabled = checkBox1.Enabled = true;
                label5.ForeColor = Color.FromArgb(221, 195, 158);
            }
            else
            {
                textBox1.Enabled = textBox2.Enabled = checkBox1.Enabled = false;
                label5.ForeColor = Color.FromArgb(103, 109, 109);
            }
        }
        #endregion

        #region Копирайт
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://vk.com/isriko");
        }
        #endregion

        #region Отключено
        //Добавление своей записи
        private void toolStripTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (toolStripTextBox1.Text == "") return;
                e.SuppressKeyPress = true;

                delPos();
                nowBox.Image = Properties.Resources.def;
                toolTip1.SetToolTip(nowBox, toolStripTextBox1.Text);

                ListViewItem listViewItem = new ListViewItem(toolStripTextBox1.Text, 28);
                listViewItem.Name = toolStripTextBox1.Text;
                listView1.Items.Add(listViewItem);

                contextMenuStrip1.Hide();
                toolStripTextBox1.Text = "";
            }

        }


        #endregion

        
    }
}
