﻿namespace BDOMap
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList4lvl = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItem4lvl = new System.Windows.Forms.ToolStripMenuItem();
            this.бронзовыйПодсвечникToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.затвердевшаяЛаваToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.инструкцияМорякаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.клубокРазноцветныхНитейToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.копьеОрденаТриныToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.лекарствоОтВсехБолезнейToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.осколокАметистаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.пиратскийКлючToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.статуяОбезглавленногоДраконаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.украденныйПиратскийКинжалToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.украшениеИзРакушекToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.шлемОрденаТриныToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem5lvl = new System.Windows.Forms.ToolStripMenuItem();
            this.летнийШлемникToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.восьмигранныйСундукToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.засушеннаяБабочкаФалланToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зельеМолодостиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.неизвестнаяГорнаяПородаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.портретДревнегоЧеловекаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.синийКварцToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.слезаСтатуиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тридцатисемилетняяНастойкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чешуяЗолотистойРыбкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чучелоБелойЛичинкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.прочееToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чистыйАлкогольToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запеченнаяПтицаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пивоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.уксусToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.плодПриродыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.box11_4 = new System.Windows.Forms.PictureBox();
            this.box10_4 = new System.Windows.Forms.PictureBox();
            this.box9_4 = new System.Windows.Forms.PictureBox();
            this.box8_4 = new System.Windows.Forms.PictureBox();
            this.box7_4 = new System.Windows.Forms.PictureBox();
            this.box6_4 = new System.Windows.Forms.PictureBox();
            this.box5_4 = new System.Windows.Forms.PictureBox();
            this.box4_4 = new System.Windows.Forms.PictureBox();
            this.box3_3 = new System.Windows.Forms.PictureBox();
            this.other = new System.Windows.Forms.PictureBox();
            this.box2_3 = new System.Windows.Forms.PictureBox();
            this.box1_3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxTarget = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box11_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box10_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box9_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box8_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box7_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box6_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box5_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box4_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box3_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.other)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box2_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box1_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pictureBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTarget)).BeginInit();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.AutoArrange = false;
            this.listView1.BackColor = System.Drawing.Color.DarkGray;
            this.listView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listView1.HideSelection = false;
            this.listView1.LabelEdit = true;
            this.listView1.LabelWrap = false;
            this.listView1.LargeImageList = this.imageList4lvl;
            this.listView1.Location = new System.Drawing.Point(739, 94);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.ShowGroups = false;
            this.listView1.Size = new System.Drawing.Size(275, 455);
            this.listView1.SmallImageList = this.imageList4lvl;
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Tile;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 30;
            // 
            // columnHeader2
            // 
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imageList4lvl
            // 
            this.imageList4lvl.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4lvl.ImageStream")));
            this.imageList4lvl.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList4lvl.Images.SetKeyName(0, "Бронзовый подсвечник.png");
            this.imageList4lvl.Images.SetKeyName(1, "Глыба голубой соли.png");
            this.imageList4lvl.Images.SetKeyName(2, "Затвердевшая лава.png");
            this.imageList4lvl.Images.SetKeyName(3, "Инструкция моряка.png");
            this.imageList4lvl.Images.SetKeyName(4, "Клубок разноцветных нитей.png");
            this.imageList4lvl.Images.SetKeyName(5, "Копье ордена Трины.png");
            this.imageList4lvl.Images.SetKeyName(6, "Лекарство от всех болезней.png");
            this.imageList4lvl.Images.SetKeyName(7, "Осколок аметиста.png");
            this.imageList4lvl.Images.SetKeyName(8, "Пиратский ключ.png");
            this.imageList4lvl.Images.SetKeyName(9, "Старый сундук с золотыми монетами.png");
            this.imageList4lvl.Images.SetKeyName(10, "Статуя обезглавленного дракона.png");
            this.imageList4lvl.Images.SetKeyName(11, "Украденный пиратский кинжал.png");
            this.imageList4lvl.Images.SetKeyName(12, "Украшение из ракушек.png");
            this.imageList4lvl.Images.SetKeyName(13, "Шлем ордена Трины.png");
            this.imageList4lvl.Images.SetKeyName(14, "102-летний шлемник.png");
            this.imageList4lvl.Images.SetKeyName(15, "Восьмигранный сундук.png");
            this.imageList4lvl.Images.SetKeyName(16, "Выцветшая статуя золотого дракона.png");
            this.imageList4lvl.Images.SetKeyName(17, "Засушенная бабочка фаллан.png");
            this.imageList4lvl.Images.SetKeyName(18, "Зелье молодости.png");
            this.imageList4lvl.Images.SetKeyName(19, "Золотой подсвечник высокого качества.png");
            this.imageList4lvl.Images.SetKeyName(20, "Неизвестная горная порода.png");
            this.imageList4lvl.Images.SetKeyName(21, "Портрет древнего человека.png");
            this.imageList4lvl.Images.SetKeyName(22, "Синий кварц.png");
            this.imageList4lvl.Images.SetKeyName(23, "Слеза статуи.png");
            this.imageList4lvl.Images.SetKeyName(24, "Тридцатисемилетняя настойка.png");
            this.imageList4lvl.Images.SetKeyName(25, "Узорчатая ткань высокого качества.png");
            this.imageList4lvl.Images.SetKeyName(26, "Чешуя золотистой рыбки.png");
            this.imageList4lvl.Images.SetKeyName(27, "Чучело белой личинки.png");
            this.imageList4lvl.Images.SetKeyName(28, "def.png");
            this.imageList4lvl.Images.SetKeyName(29, "Чистый алкоголь.png");
            this.imageList4lvl.Images.SetKeyName(30, "Запеченная птица.png");
            this.imageList4lvl.Images.SetKeyName(31, "Пиво.png");
            this.imageList4lvl.Images.SetKeyName(32, "Уксус.png");
            this.imageList4lvl.Images.SetKeyName(33, "Плод природы.png");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem4lvl,
            this.ToolStripMenuItem5lvl,
            this.прочееToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripTextBox1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(161, 101);
            // 
            // ToolStripMenuItem4lvl
            // 
            this.ToolStripMenuItem4lvl.BackColor = System.Drawing.Color.Gold;
            this.ToolStripMenuItem4lvl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToolStripMenuItem4lvl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.бронзовыйПодсвечникToolStripMenuItem1,
            this.gfToolStripMenuItem,
            this.затвердевшаяЛаваToolStripMenuItem1,
            this.инструкцияМорякаToolStripMenuItem1,
            this.клубокРазноцветныхНитейToolStripMenuItem1,
            this.копьеОрденаТриныToolStripMenuItem1,
            this.лекарствоОтВсехБолезнейToolStripMenuItem1,
            this.осколокАметистаToolStripMenuItem1,
            this.пиратскийКлючToolStripMenuItem1,
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1,
            this.статуяОбезглавленногоДраконаToolStripMenuItem1,
            this.украденныйПиратскийКинжалToolStripMenuItem1,
            this.украшениеИзРакушекToolStripMenuItem1,
            this.шлемОрденаТриныToolStripMenuItem1});
            this.ToolStripMenuItem4lvl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ToolStripMenuItem4lvl.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ToolStripMenuItem4lvl.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ToolStripMenuItem4lvl.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripMenuItem4lvl.Name = "ToolStripMenuItem4lvl";
            this.ToolStripMenuItem4lvl.Size = new System.Drawing.Size(160, 22);
            this.ToolStripMenuItem4lvl.Text = "[ 4 ур. ]";
            // 
            // бронзовыйПодсвечникToolStripMenuItem1
            // 
            this.бронзовыйПодсвечникToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.бронзовыйПодсвечникToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.бронзовыйПодсвечникToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Бронзовый_подсвечник;
            this.бронзовыйПодсвечникToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.бронзовыйПодсвечникToolStripMenuItem1.Name = "бронзовыйПодсвечникToolStripMenuItem1";
            this.бронзовыйПодсвечникToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.бронзовыйПодсвечникToolStripMenuItem1.Tag = "0";
            this.бронзовыйПодсвечникToolStripMenuItem1.Text = "Бронзовый подсвечник";
            this.бронзовыйПодсвечникToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // gfToolStripMenuItem
            // 
            this.gfToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.gfToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gfToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Глыба_голубой_соли;
            this.gfToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.gfToolStripMenuItem.Name = "gfToolStripMenuItem";
            this.gfToolStripMenuItem.Size = new System.Drawing.Size(294, 50);
            this.gfToolStripMenuItem.Tag = "1";
            this.gfToolStripMenuItem.Text = "Глыба голубой соли";
            this.gfToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // затвердевшаяЛаваToolStripMenuItem1
            // 
            this.затвердевшаяЛаваToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.затвердевшаяЛаваToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.затвердевшаяЛаваToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Затвердевшая_лава;
            this.затвердевшаяЛаваToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.затвердевшаяЛаваToolStripMenuItem1.Name = "затвердевшаяЛаваToolStripMenuItem1";
            this.затвердевшаяЛаваToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.затвердевшаяЛаваToolStripMenuItem1.Tag = "2";
            this.затвердевшаяЛаваToolStripMenuItem1.Text = "Затвердевшая лава";
            this.затвердевшаяЛаваToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // инструкцияМорякаToolStripMenuItem1
            // 
            this.инструкцияМорякаToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.инструкцияМорякаToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.инструкцияМорякаToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Инструкция_моряка;
            this.инструкцияМорякаToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.инструкцияМорякаToolStripMenuItem1.Name = "инструкцияМорякаToolStripMenuItem1";
            this.инструкцияМорякаToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.инструкцияМорякаToolStripMenuItem1.Tag = "3";
            this.инструкцияМорякаToolStripMenuItem1.Text = "Инструкция моряка";
            this.инструкцияМорякаToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // клубокРазноцветныхНитейToolStripMenuItem1
            // 
            this.клубокРазноцветныхНитейToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.клубокРазноцветныхНитейToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.клубокРазноцветныхНитейToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Клубок_разноцветных_нитей;
            this.клубокРазноцветныхНитейToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.клубокРазноцветныхНитейToolStripMenuItem1.Name = "клубокРазноцветныхНитейToolStripMenuItem1";
            this.клубокРазноцветныхНитейToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.клубокРазноцветныхНитейToolStripMenuItem1.Tag = "4";
            this.клубокРазноцветныхНитейToolStripMenuItem1.Text = "Клубок разноцветных нитей";
            this.клубокРазноцветныхНитейToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // копьеОрденаТриныToolStripMenuItem1
            // 
            this.копьеОрденаТриныToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.копьеОрденаТриныToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.копьеОрденаТриныToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Копье_ордена_Трины;
            this.копьеОрденаТриныToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.копьеОрденаТриныToolStripMenuItem1.Name = "копьеОрденаТриныToolStripMenuItem1";
            this.копьеОрденаТриныToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.копьеОрденаТриныToolStripMenuItem1.Tag = "5";
            this.копьеОрденаТриныToolStripMenuItem1.Text = "Копье ордена Трины";
            this.копьеОрденаТриныToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // лекарствоОтВсехБолезнейToolStripMenuItem1
            // 
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Лекарство_от_всех_болезней;
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.Name = "лекарствоОтВсехБолезнейToolStripMenuItem1";
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.Tag = "6";
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.Text = "Лекарство от всех болезней";
            this.лекарствоОтВсехБолезнейToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // осколокАметистаToolStripMenuItem1
            // 
            this.осколокАметистаToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.осколокАметистаToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.осколокАметистаToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Осколок_аметиста;
            this.осколокАметистаToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.осколокАметистаToolStripMenuItem1.Name = "осколокАметистаToolStripMenuItem1";
            this.осколокАметистаToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.осколокАметистаToolStripMenuItem1.Tag = "7";
            this.осколокАметистаToolStripMenuItem1.Text = "Осколок аметиста";
            this.осколокАметистаToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // пиратскийКлючToolStripMenuItem1
            // 
            this.пиратскийКлючToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.пиратскийКлючToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.пиратскийКлючToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Пиратский_ключ;
            this.пиратскийКлючToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.пиратскийКлючToolStripMenuItem1.Name = "пиратскийКлючToolStripMenuItem1";
            this.пиратскийКлючToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.пиратскийКлючToolStripMenuItem1.Tag = "8";
            this.пиратскийКлючToolStripMenuItem1.Text = "Пиратский ключ";
            this.пиратскийКлючToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // старыйСундукСЗолотымиМонетамиToolStripMenuItem1
            // 
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Старый_сундук_с_золотыми_монетами;
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.Name = "старыйСундукСЗолотымиМонетамиToolStripMenuItem1";
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.Tag = "9";
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.Text = "Старый сундук с золотыми монетами";
            this.старыйСундукСЗолотымиМонетамиToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // статуяОбезглавленногоДраконаToolStripMenuItem1
            // 
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Статуя_обезглавленного_дракона;
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.Name = "статуяОбезглавленногоДраконаToolStripMenuItem1";
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.Tag = "10";
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.Text = "Статуя обезглавленного дракона";
            this.статуяОбезглавленногоДраконаToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // украденныйПиратскийКинжалToolStripMenuItem1
            // 
            this.украденныйПиратскийКинжалToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.украденныйПиратскийКинжалToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.украденныйПиратскийКинжалToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Украденный_пиратский_кинжал;
            this.украденныйПиратскийКинжалToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.украденныйПиратскийКинжалToolStripMenuItem1.Name = "украденныйПиратскийКинжалToolStripMenuItem1";
            this.украденныйПиратскийКинжалToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.украденныйПиратскийКинжалToolStripMenuItem1.Tag = "11";
            this.украденныйПиратскийКинжалToolStripMenuItem1.Text = "Украденный пиратский кинжал";
            this.украденныйПиратскийКинжалToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // украшениеИзРакушекToolStripMenuItem1
            // 
            this.украшениеИзРакушекToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.украшениеИзРакушекToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.украшениеИзРакушекToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Украшение_из_ракушек;
            this.украшениеИзРакушекToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.украшениеИзРакушекToolStripMenuItem1.Name = "украшениеИзРакушекToolStripMenuItem1";
            this.украшениеИзРакушекToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.украшениеИзРакушекToolStripMenuItem1.Tag = "12";
            this.украшениеИзРакушекToolStripMenuItem1.Text = "Украшение из ракушек";
            this.украшениеИзРакушекToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // шлемОрденаТриныToolStripMenuItem1
            // 
            this.шлемОрденаТриныToolStripMenuItem1.BackColor = System.Drawing.Color.Silver;
            this.шлемОрденаТриныToolStripMenuItem1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.шлемОрденаТриныToolStripMenuItem1.Image = global::BDOMap.Properties.Resources.Шлем_ордена_Трины;
            this.шлемОрденаТриныToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.шлемОрденаТриныToolStripMenuItem1.Name = "шлемОрденаТриныToolStripMenuItem1";
            this.шлемОрденаТриныToolStripMenuItem1.Size = new System.Drawing.Size(294, 50);
            this.шлемОрденаТриныToolStripMenuItem1.Tag = "13";
            this.шлемОрденаТриныToolStripMenuItem1.Text = "Шлем ордена Трины";
            this.шлемОрденаТриныToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // ToolStripMenuItem5lvl
            // 
            this.ToolStripMenuItem5lvl.BackColor = System.Drawing.Color.Orange;
            this.ToolStripMenuItem5lvl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.летнийШлемникToolStripMenuItem,
            this.восьмигранныйСундукToolStripMenuItem,
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem,
            this.засушеннаяБабочкаФалланToolStripMenuItem,
            this.зельеМолодостиToolStripMenuItem,
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem,
            this.неизвестнаяГорнаяПородаToolStripMenuItem,
            this.портретДревнегоЧеловекаToolStripMenuItem,
            this.синийКварцToolStripMenuItem,
            this.слезаСтатуиToolStripMenuItem,
            this.тридцатисемилетняяНастойкаToolStripMenuItem,
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem,
            this.чешуяЗолотистойРыбкиToolStripMenuItem,
            this.чучелоБелойЛичинкиToolStripMenuItem});
            this.ToolStripMenuItem5lvl.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ToolStripMenuItem5lvl.ForeColor = System.Drawing.SystemColors.InfoText;
            this.ToolStripMenuItem5lvl.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripMenuItem5lvl.Name = "ToolStripMenuItem5lvl";
            this.ToolStripMenuItem5lvl.Size = new System.Drawing.Size(160, 22);
            this.ToolStripMenuItem5lvl.Text = "[ 5 ур. ]";
            // 
            // летнийШлемникToolStripMenuItem
            // 
            this.летнийШлемникToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.летнийШлемникToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.летнийШлемникToolStripMenuItem.Image = global::BDOMap.Properties.Resources._102_летний_шлемник;
            this.летнийШлемникToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.летнийШлемникToolStripMenuItem.Name = "летнийШлемникToolStripMenuItem";
            this.летнийШлемникToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.летнийШлемникToolStripMenuItem.Tag = "14";
            this.летнийШлемникToolStripMenuItem.Text = "102-летний шлемник";
            this.летнийШлемникToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // восьмигранныйСундукToolStripMenuItem
            // 
            this.восьмигранныйСундукToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.восьмигранныйСундукToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.восьмигранныйСундукToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Восьмигранный_сундук;
            this.восьмигранныйСундукToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.восьмигранныйСундукToolStripMenuItem.Name = "восьмигранныйСундукToolStripMenuItem";
            this.восьмигранныйСундукToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.восьмигранныйСундукToolStripMenuItem.Tag = "15";
            this.восьмигранныйСундукToolStripMenuItem.Text = "Восьмигранный сундук";
            this.восьмигранныйСундукToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem
            // 
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Выцветшая_статуя_золотого_дракона;
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.Name = "выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem";
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.Tag = "16";
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.Text = "Выцветшая статуя золотого дракона";
            this.выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // засушеннаяБабочкаФалланToolStripMenuItem
            // 
            this.засушеннаяБабочкаФалланToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.засушеннаяБабочкаФалланToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.засушеннаяБабочкаФалланToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Засушенная_бабочка_фаллан;
            this.засушеннаяБабочкаФалланToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.засушеннаяБабочкаФалланToolStripMenuItem.Name = "засушеннаяБабочкаФалланToolStripMenuItem";
            this.засушеннаяБабочкаФалланToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.засушеннаяБабочкаФалланToolStripMenuItem.Tag = "17";
            this.засушеннаяБабочкаФалланToolStripMenuItem.Text = "Засушенная бабочка фаллан";
            this.засушеннаяБабочкаФалланToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // зельеМолодостиToolStripMenuItem
            // 
            this.зельеМолодостиToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.зельеМолодостиToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.зельеМолодостиToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Зелье_молодости;
            this.зельеМолодостиToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.зельеМолодостиToolStripMenuItem.Name = "зельеМолодостиToolStripMenuItem";
            this.зельеМолодостиToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.зельеМолодостиToolStripMenuItem.Tag = "18";
            this.зельеМолодостиToolStripMenuItem.Text = "Зелье молодости";
            this.зельеМолодостиToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // золотойПодсвечникВысокогоКачестваToolStripMenuItem
            // 
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Золотой_подсвечник_высокого_качества;
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.Name = "золотойПодсвечникВысокогоКачестваToolStripMenuItem";
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.Tag = "19";
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.Text = "Золотой подсвечник высокого качества";
            this.золотойПодсвечникВысокогоКачестваToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // неизвестнаяГорнаяПородаToolStripMenuItem
            // 
            this.неизвестнаяГорнаяПородаToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.неизвестнаяГорнаяПородаToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.неизвестнаяГорнаяПородаToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Неизвестная_горная_порода;
            this.неизвестнаяГорнаяПородаToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.неизвестнаяГорнаяПородаToolStripMenuItem.Name = "неизвестнаяГорнаяПородаToolStripMenuItem";
            this.неизвестнаяГорнаяПородаToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.неизвестнаяГорнаяПородаToolStripMenuItem.Tag = "20";
            this.неизвестнаяГорнаяПородаToolStripMenuItem.Text = "Неизвестная горная порода";
            this.неизвестнаяГорнаяПородаToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // портретДревнегоЧеловекаToolStripMenuItem
            // 
            this.портретДревнегоЧеловекаToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.портретДревнегоЧеловекаToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.портретДревнегоЧеловекаToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Портрет_древнего_человека;
            this.портретДревнегоЧеловекаToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.портретДревнегоЧеловекаToolStripMenuItem.Name = "портретДревнегоЧеловекаToolStripMenuItem";
            this.портретДревнегоЧеловекаToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.портретДревнегоЧеловекаToolStripMenuItem.Tag = "21";
            this.портретДревнегоЧеловекаToolStripMenuItem.Text = "Портрет древнего человека";
            this.портретДревнегоЧеловекаToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // синийКварцToolStripMenuItem
            // 
            this.синийКварцToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.синийКварцToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.синийКварцToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Синий_кварц;
            this.синийКварцToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.синийКварцToolStripMenuItem.Name = "синийКварцToolStripMenuItem";
            this.синийКварцToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.синийКварцToolStripMenuItem.Tag = "22";
            this.синийКварцToolStripMenuItem.Text = "Синий кварц";
            this.синийКварцToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // слезаСтатуиToolStripMenuItem
            // 
            this.слезаСтатуиToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.слезаСтатуиToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.слезаСтатуиToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Слеза_статуи;
            this.слезаСтатуиToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.слезаСтатуиToolStripMenuItem.Name = "слезаСтатуиToolStripMenuItem";
            this.слезаСтатуиToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.слезаСтатуиToolStripMenuItem.Tag = "23";
            this.слезаСтатуиToolStripMenuItem.Text = "Слеза статуи";
            this.слезаСтатуиToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // тридцатисемилетняяНастойкаToolStripMenuItem
            // 
            this.тридцатисемилетняяНастойкаToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.тридцатисемилетняяНастойкаToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.тридцатисемилетняяНастойкаToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Тридцатисемилетняя_настойка;
            this.тридцатисемилетняяНастойкаToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.тридцатисемилетняяНастойкаToolStripMenuItem.Name = "тридцатисемилетняяНастойкаToolStripMenuItem";
            this.тридцатисемилетняяНастойкаToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.тридцатисемилетняяНастойкаToolStripMenuItem.Tag = "24";
            this.тридцатисемилетняяНастойкаToolStripMenuItem.Text = "Тридцатисемилетняя настойка";
            this.тридцатисемилетняяНастойкаToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // узорчатаяТканьВысокогоКачестваToolStripMenuItem
            // 
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Узорчатая_ткань_высокого_качества;
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.Name = "узорчатаяТканьВысокогоКачестваToolStripMenuItem";
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.Tag = "25";
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.Text = "Узорчатая ткань высокого качества";
            this.узорчатаяТканьВысокогоКачестваToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // чешуяЗолотистойРыбкиToolStripMenuItem
            // 
            this.чешуяЗолотистойРыбкиToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.чешуяЗолотистойРыбкиToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.чешуяЗолотистойРыбкиToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Чешуя_золотистой_рыбки;
            this.чешуяЗолотистойРыбкиToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.чешуяЗолотистойРыбкиToolStripMenuItem.Name = "чешуяЗолотистойРыбкиToolStripMenuItem";
            this.чешуяЗолотистойРыбкиToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.чешуяЗолотистойРыбкиToolStripMenuItem.Tag = "26";
            this.чешуяЗолотистойРыбкиToolStripMenuItem.Text = "Чешуя золотистой рыбки";
            this.чешуяЗолотистойРыбкиToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // чучелоБелойЛичинкиToolStripMenuItem
            // 
            this.чучелоБелойЛичинкиToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.чучелоБелойЛичинкиToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.чучелоБелойЛичинкиToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Чучело_белой_личинки;
            this.чучелоБелойЛичинкиToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.чучелоБелойЛичинкиToolStripMenuItem.Name = "чучелоБелойЛичинкиToolStripMenuItem";
            this.чучелоБелойЛичинкиToolStripMenuItem.Size = new System.Drawing.Size(324, 50);
            this.чучелоБелойЛичинкиToolStripMenuItem.Tag = "27";
            this.чучелоБелойЛичинкиToolStripMenuItem.Text = "Чучело белой личинки";
            this.чучелоБелойЛичинкиToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // прочееToolStripMenuItem
            // 
            this.прочееToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.прочееToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.чистыйАлкогольToolStripMenuItem,
            this.запеченнаяПтицаToolStripMenuItem,
            this.пивоToolStripMenuItem,
            this.уксусToolStripMenuItem,
            this.плодПриродыToolStripMenuItem});
            this.прочееToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.прочееToolStripMenuItem.Name = "прочееToolStripMenuItem";
            this.прочееToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.прочееToolStripMenuItem.Text = "Прочее";
            // 
            // чистыйАлкогольToolStripMenuItem
            // 
            this.чистыйАлкогольToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.чистыйАлкогольToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.чистыйАлкогольToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("чистыйАлкогольToolStripMenuItem.Image")));
            this.чистыйАлкогольToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.чистыйАлкогольToolStripMenuItem.Name = "чистыйАлкогольToolStripMenuItem";
            this.чистыйАлкогольToolStripMenuItem.Size = new System.Drawing.Size(202, 50);
            this.чистыйАлкогольToolStripMenuItem.Tag = "29";
            this.чистыйАлкогольToolStripMenuItem.Text = "Чистый алкоголь";
            this.чистыйАлкогольToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // запеченнаяПтицаToolStripMenuItem
            // 
            this.запеченнаяПтицаToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.запеченнаяПтицаToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.запеченнаяПтицаToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Запеченная_птица;
            this.запеченнаяПтицаToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.запеченнаяПтицаToolStripMenuItem.Name = "запеченнаяПтицаToolStripMenuItem";
            this.запеченнаяПтицаToolStripMenuItem.Size = new System.Drawing.Size(202, 50);
            this.запеченнаяПтицаToolStripMenuItem.Tag = "30";
            this.запеченнаяПтицаToolStripMenuItem.Text = "Запеченная птица";
            this.запеченнаяПтицаToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // пивоToolStripMenuItem
            // 
            this.пивоToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.пивоToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.пивоToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Пиво;
            this.пивоToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.пивоToolStripMenuItem.Name = "пивоToolStripMenuItem";
            this.пивоToolStripMenuItem.Size = new System.Drawing.Size(202, 50);
            this.пивоToolStripMenuItem.Tag = "31";
            this.пивоToolStripMenuItem.Text = "Пиво";
            this.пивоToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // уксусToolStripMenuItem
            // 
            this.уксусToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.уксусToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.уксусToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Уксус;
            this.уксусToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.уксусToolStripMenuItem.Name = "уксусToolStripMenuItem";
            this.уксусToolStripMenuItem.Size = new System.Drawing.Size(202, 50);
            this.уксусToolStripMenuItem.Tag = "32";
            this.уксусToolStripMenuItem.Text = "Уксус";
            this.уксусToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // плодПриродыToolStripMenuItem
            // 
            this.плодПриродыToolStripMenuItem.BackColor = System.Drawing.Color.Silver;
            this.плодПриродыToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.плодПриродыToolStripMenuItem.Image = global::BDOMap.Properties.Resources.Плод_природы;
            this.плодПриродыToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.плодПриродыToolStripMenuItem.Name = "плодПриродыToolStripMenuItem";
            this.плодПриродыToolStripMenuItem.Size = new System.Drawing.Size(202, 50);
            this.плодПриродыToolStripMenuItem.Tag = "33";
            this.плодПриродыToolStripMenuItem.Text = "Плод природы";
            this.плодПриродыToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(157, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Visible = false;
            this.toolStripTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripTextBox1_KeyDown);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 2500;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(999, 556);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 8;
            this.toolTip1.SetToolTip(this.checkBox1, "Активный премиум набор (-10%)");
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(195)))), ((int)(((byte)(158)))));
            this.label1.Location = new System.Drawing.Point(907, 578);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "0 /";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(73)))), ((int)(((byte)(73)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(195)))), ((int)(((byte)(158)))));
            this.textBox1.Location = new System.Drawing.Point(972, 579);
            this.textBox1.MaxLength = 5;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(42, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "10770";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Location = new System.Drawing.Point(740, 608);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(274, 13);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(130)))), ((int)(((byte)(87)))));
            this.panel2.Location = new System.Drawing.Point(740, 608);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(50, 13);
            this.panel2.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(195)))), ((int)(((byte)(158)))));
            this.label3.Location = new System.Drawing.Point(739, 552);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Убеждение:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(73)))), ((int)(((byte)(73)))));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(195)))), ((int)(((byte)(158)))));
            this.textBox2.Location = new System.Drawing.Point(935, 553);
            this.textBox2.MaxLength = 5;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(42, 20);
            this.textBox2.TabIndex = 6;
            this.textBox2.Text = "15.15";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(195)))), ((int)(((byte)(158)))));
            this.label4.Location = new System.Drawing.Point(739, 578);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "Грузоподъёмность:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(195)))), ((int)(((byte)(158)))));
            this.label5.Location = new System.Drawing.Point(978, 553);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 19);
            this.label5.TabIndex = 5;
            this.label5.Text = "%";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(195)))), ((int)(((byte)(158)))));
            this.label2.Location = new System.Drawing.Point(836, 552);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "0";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::BDOMap.Properties.Resources.logo;
            this.pictureBox12.Location = new System.Drawing.Point(740, 13);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(274, 75);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 4;
            this.pictureBox12.TabStop = false;
            // 
            // box11_4
            // 
            this.box11_4.BackColor = System.Drawing.Color.Azure;
            this.box11_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box11_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box11_4.Location = new System.Drawing.Point(183, 507);
            this.box11_4.Margin = new System.Windows.Forms.Padding(2);
            this.box11_4.Name = "box11_4";
            this.box11_4.Padding = new System.Windows.Forms.Padding(2);
            this.box11_4.Size = new System.Drawing.Size(33, 33);
            this.box11_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box11_4.TabIndex = 3;
            this.box11_4.TabStop = false;
            this.box11_4.Tag = "58180";
            this.box11_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box10_4
            // 
            this.box10_4.BackColor = System.Drawing.Color.Azure;
            this.box10_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box10_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box10_4.Location = new System.Drawing.Point(52, 472);
            this.box10_4.Margin = new System.Windows.Forms.Padding(2);
            this.box10_4.Name = "box10_4";
            this.box10_4.Padding = new System.Windows.Forms.Padding(2);
            this.box10_4.Size = new System.Drawing.Size(33, 33);
            this.box10_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box10_4.TabIndex = 3;
            this.box10_4.TabStop = false;
            this.box10_4.Tag = "58180";
            this.box10_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box9_4
            // 
            this.box9_4.BackColor = System.Drawing.Color.Azure;
            this.box9_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box9_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box9_4.Location = new System.Drawing.Point(136, 393);
            this.box9_4.Margin = new System.Windows.Forms.Padding(2);
            this.box9_4.Name = "box9_4";
            this.box9_4.Padding = new System.Windows.Forms.Padding(2);
            this.box9_4.Size = new System.Drawing.Size(33, 33);
            this.box9_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box9_4.TabIndex = 3;
            this.box9_4.TabStop = false;
            this.box9_4.Tag = "58180";
            this.box9_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box8_4
            // 
            this.box8_4.BackColor = System.Drawing.Color.Azure;
            this.box8_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box8_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box8_4.Location = new System.Drawing.Point(243, 306);
            this.box8_4.Margin = new System.Windows.Forms.Padding(2);
            this.box8_4.Name = "box8_4";
            this.box8_4.Padding = new System.Windows.Forms.Padding(2);
            this.box8_4.Size = new System.Drawing.Size(33, 33);
            this.box8_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box8_4.TabIndex = 3;
            this.box8_4.TabStop = false;
            this.box8_4.Tag = "58180";
            this.box8_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box7_4
            // 
            this.box7_4.BackColor = System.Drawing.Color.Azure;
            this.box7_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box7_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box7_4.Location = new System.Drawing.Point(340, 265);
            this.box7_4.Margin = new System.Windows.Forms.Padding(2);
            this.box7_4.Name = "box7_4";
            this.box7_4.Padding = new System.Windows.Forms.Padding(2);
            this.box7_4.Size = new System.Drawing.Size(33, 33);
            this.box7_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box7_4.TabIndex = 3;
            this.box7_4.TabStop = false;
            this.box7_4.Tag = "58180";
            this.box7_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box6_4
            // 
            this.box6_4.BackColor = System.Drawing.Color.Azure;
            this.box6_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box6_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box6_4.Location = new System.Drawing.Point(114, 246);
            this.box6_4.Margin = new System.Windows.Forms.Padding(2);
            this.box6_4.Name = "box6_4";
            this.box6_4.Padding = new System.Windows.Forms.Padding(2);
            this.box6_4.Size = new System.Drawing.Size(33, 33);
            this.box6_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box6_4.TabIndex = 3;
            this.box6_4.TabStop = false;
            this.box6_4.Tag = "58180";
            this.box6_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box5_4
            // 
            this.box5_4.BackColor = System.Drawing.Color.Azure;
            this.box5_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box5_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box5_4.Location = new System.Drawing.Point(319, 156);
            this.box5_4.Margin = new System.Windows.Forms.Padding(2);
            this.box5_4.Name = "box5_4";
            this.box5_4.Padding = new System.Windows.Forms.Padding(2);
            this.box5_4.Size = new System.Drawing.Size(33, 33);
            this.box5_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box5_4.TabIndex = 3;
            this.box5_4.TabStop = false;
            this.box5_4.Tag = "58180";
            this.box5_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box4_4
            // 
            this.box4_4.BackColor = System.Drawing.Color.Azure;
            this.box4_4.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box4_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box4_4.Location = new System.Drawing.Point(458, 69);
            this.box4_4.Margin = new System.Windows.Forms.Padding(2);
            this.box4_4.Name = "box4_4";
            this.box4_4.Padding = new System.Windows.Forms.Padding(2);
            this.box4_4.Size = new System.Drawing.Size(33, 33);
            this.box4_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box4_4.TabIndex = 3;
            this.box4_4.TabStop = false;
            this.box4_4.Tag = "58180";
            this.box4_4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box3_3
            // 
            this.box3_3.BackColor = System.Drawing.Color.Azure;
            this.box3_3.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box3_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box3_3.Location = new System.Drawing.Point(258, 86);
            this.box3_3.Margin = new System.Windows.Forms.Padding(2);
            this.box3_3.Name = "box3_3";
            this.box3_3.Padding = new System.Windows.Forms.Padding(2);
            this.box3_3.Size = new System.Drawing.Size(33, 33);
            this.box3_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box3_3.TabIndex = 3;
            this.box3_3.TabStop = false;
            this.box3_3.Tag = "46544";
            this.box3_3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // other
            // 
            this.other.BackColor = System.Drawing.Color.Azure;
            this.other.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.other.Cursor = System.Windows.Forms.Cursors.Hand;
            this.other.Location = new System.Drawing.Point(129, 94);
            this.other.Margin = new System.Windows.Forms.Padding(2);
            this.other.Name = "other";
            this.other.Padding = new System.Windows.Forms.Padding(2);
            this.other.Size = new System.Drawing.Size(33, 33);
            this.other.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.other.TabIndex = 3;
            this.other.TabStop = false;
            this.other.Tag = "14282";
            this.other.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box2_3
            // 
            this.box2_3.BackColor = System.Drawing.Color.Azure;
            this.box2_3.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box2_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box2_3.Location = new System.Drawing.Point(25, 119);
            this.box2_3.Margin = new System.Windows.Forms.Padding(2);
            this.box2_3.Name = "box2_3";
            this.box2_3.Padding = new System.Windows.Forms.Padding(2);
            this.box2_3.Size = new System.Drawing.Size(33, 33);
            this.box2_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box2_3.TabIndex = 3;
            this.box2_3.TabStop = false;
            this.box2_3.Tag = "46544";
            this.box2_3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // box1_3
            // 
            this.box1_3.BackColor = System.Drawing.Color.Azure;
            this.box1_3.BackgroundImage = global::BDOMap.Properties.Resources.sq;
            this.box1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.box1_3.Location = new System.Drawing.Point(205, 23);
            this.box1_3.Margin = new System.Windows.Forms.Padding(2);
            this.box1_3.Name = "box1_3";
            this.box1_3.Padding = new System.Windows.Forms.Padding(2);
            this.box1_3.Size = new System.Drawing.Size(33, 33);
            this.box1_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.box1_3.TabIndex = 2;
            this.box1_3.TabStop = false;
            this.box1_3.Tag = "46544";
            this.box1_3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.openContextMenu);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Controls.Add(this.pictureBoxTarget);
            this.pictureBox1.Controls.Add(this.linkLabel1);
            this.pictureBox1.Controls.Add(this.label6);
            this.pictureBox1.Image = global::BDOMap.Properties.Resources.map;
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(720, 610);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBoxTarget
            // 
            this.pictureBoxTarget.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxTarget.Image = global::BDOMap.Properties.Resources.target70;
            this.pictureBoxTarget.Location = new System.Drawing.Point(95, 61);
            this.pictureBoxTarget.Name = "pictureBoxTarget";
            this.pictureBoxTarget.Size = new System.Drawing.Size(70, 70);
            this.pictureBoxTarget.TabIndex = 4;
            this.pictureBoxTarget.TabStop = false;
            this.pictureBoxTarget.Visible = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.LinkColor = System.Drawing.Color.Brown;
            this.linkLabel1.Location = new System.Drawing.Point(562, 586);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(51, 13);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "by IsRiko";
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.Brown;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.01F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(255)));
            this.label6.ForeColor = System.Drawing.Color.Gainsboro;
            this.label6.Location = new System.Drawing.Point(283, 322);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 19);
            this.label6.TabIndex = 9;
            this.label6.Text = "Затонувший корабль";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(73)))), ((int)(((byte)(73)))));
            this.ClientSize = new System.Drawing.Size(1026, 635);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.box11_4);
            this.Controls.Add(this.box10_4);
            this.Controls.Add(this.box9_4);
            this.Controls.Add(this.box8_4);
            this.Controls.Add(this.box7_4);
            this.Controls.Add(this.box6_4);
            this.Controls.Add(this.box5_4);
            this.Controls.Add(this.box4_4);
            this.Controls.Add(this.box3_3);
            this.Controls.Add(this.other);
            this.Controls.Add(this.box2_3);
            this.Controls.Add(this.box1_3);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Карта бартеров Лагри";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box11_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box10_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box9_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box8_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box7_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box6_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box5_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box4_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box3_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.other)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box2_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box1_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pictureBox1.ResumeLayout(false);
            this.pictureBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTarget)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.PictureBox box1_3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem4lvl;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem5lvl;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ImageList imageList4lvl;
        private System.Windows.Forms.PictureBox box2_3;
        private System.Windows.Forms.PictureBox other;
        private System.Windows.Forms.PictureBox box3_3;
        private System.Windows.Forms.PictureBox box4_4;
        private System.Windows.Forms.PictureBox box5_4;
        private System.Windows.Forms.PictureBox box6_4;
        private System.Windows.Forms.PictureBox box7_4;
        private System.Windows.Forms.PictureBox box8_4;
        private System.Windows.Forms.PictureBox box9_4;
        private System.Windows.Forms.PictureBox box10_4;
        private System.Windows.Forms.PictureBox box11_4;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ToolStripMenuItem gfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бронзовыйПодсвечникToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem затвердевшаяЛаваToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem инструкцияМорякаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem клубокРазноцветныхНитейToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem копьеОрденаТриныToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem лекарствоОтВсехБолезнейToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem осколокАметистаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem пиратскийКлючToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem старыйСундукСЗолотымиМонетамиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem статуяОбезглавленногоДраконаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem украденныйПиратскийКинжалToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem украшениеИзРакушекToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem шлемОрденаТриныToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem летнийШлемникToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem восьмигранныйСундукToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выцветшаяСтатуяЗолотогоДраконаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem засушеннаяБабочкаФалланToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зельеМолодостиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem золотойПодсвечникВысокогоКачестваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem неизвестнаяГорнаяПородаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem портретДревнегоЧеловекаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem синийКварцToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem слезаСтатуиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тридцатисемилетняяНастойкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem узорчатаяТканьВысокогоКачестваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чешуяЗолотистойРыбкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чучелоБелойЛичинкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.PictureBox pictureBoxTarget;
        private System.Windows.Forms.ToolStripMenuItem прочееToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чистыйАлкогольToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem запеченнаяПтицаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пивоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem уксусToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem плодПриродыToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
    }
}

